// module imports
const math = require('mathjs');
const parser = math.parser();

//---------------------------------------------------------------------------------------------------------------------------------------
// core of index.html and click event emitter. this part allows user to switch between tabs and see details of them
function setupTabs (){
    document.querySelectorAll(".tabs__button").forEach(button => {
        button.addEventListener("click", () => {
            const sideBar = button.parentElement;
            const tabsContainer = sideBar.parentElement;
            const tabNumber = button.dataset.forTab;
            const tabToActivate = tabsContainer.querySelector(`.tabs__content[data-tab="${tabNumber}"]`);

            sideBar.querySelectorAll(".tabs__button").forEach(button => {
                button.classList.remove("tabs__button--active");
            });

            tabsContainer.querySelectorAll(".tabs__content").forEach(tab => {
                tab.classList.remove("tabs__content--active");
            });

            button.classList.add("tabs__button--active");
            tabToActivate.classList.add("tabs__content--active");
        });
    });
}

// event listener to invoke setupTabs and some inner functions to change visibility of elements
document.addEventListener("DOMContentLoaded", () => {
    setupTabs();

    document.querySelectorAll(".tabs").forEach(tabsContainer => {
        tabsContainer.querySelector(".tabs__sidebar .tabs__button").click();
    });

    // dynamically hide and show some divs in solving equation tab
    document.querySelectorAll(".calcButtons").forEach(button => {
        button.addEventListener("click", () => {
            const parent = button.parentElement;

            // if no problem, class reamins hidden and here it will be changed to active 
            parent.querySelectorAll(".hidden").forEach(element => {
                element.classList.remove("hidden");
                element.classList.add("active");
            });

            // if any problem on evalution, class will be chamged to problem in code block
            parent.querySelectorAll(".problem").forEach(element => {
                element.classList.remove("problem");
                element.classList.add("hidden");
            });
        });
    });

    /*
    // getting id for copyResult function
    document.getElementsByClassName("copy_button").forEach(copyButton => {
        const copyResultEls = copyButton.parentElement.getElementsByClassName("copy_result")
        if (copyResultEls.length > 0){
            copyButton.addEventListener("click", e => {
                copyResult(copyResultEls[0])
            })
        }
    });
    */
});

//---------------------------------------------------------------------------------------------------------------------------------------
// basic math expression calculation. first tab alculation
function basicCalc (){
    try {
        const input = document.getElementById("basicExp").value.toString();
        const output = math.evaluate(input);
        document.getElementById("basicRes").value = output;
    }catch(err){
        alert(err);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// plotting graph. second tab calculation
function plotCalc(){
    try {
        const input = document.getElementById("plotExp").value.toString();
        const inputComp = math.compile(input);

        // evaluate value in a range to draw graph, create x and y values based on equation
        const xValues = math.range(-10, 10, 0.1).toArray();
        const yValues = xValues.map(function(x) {
            return inputComp.evaluate({x: x});
        });

        // to create data as array
        const trace = {
            x: xValues,
            y: yValues,
            type: 'scatter'
        }

        // layout options
        const layout = {
            autosize: false,
            width: 500,
            height: 500
        }

        // render the plot by using plotly (div id, data as array, layut options)
        const data = [trace];
        Plotly.newPlot('plotly', data, layout);
    }catch(err){
        alert(err);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// calculation integral. third tab calculation
function evaluateInteg(divID, divID2){
    try {
        let equation = document.getElementById("integralExp").value.toString();
        let variables = [];

        // getting base variable
        variables = extractVariables(equation);

        // if there is error (here we have integral problem with only one base), do not show calculation part
        if(variables.length != 1){
            setVisibility(divID, divID2);
            throw new Error ("Only one variable is needed to calculate integral! Try again.");
        } 

        // here adding variables to html dynamically
        let newStuff = document.getElementById(divID);
        newStuff.innerHTML = "";

        for(let i = 0; i < variables.length; i++){
            newStuff.innerHTML = `<label style="color: #06477c;"><b>Integral based on "${variables[i]}"</b></label><br></br>`;
        }
    }catch(err){
        alert(err);
    }
}

function integralCalc(){
    try {
        let equation = document.getElementById("integralExp").value.toString();
        let variables = [];
        let lower = Number(document.getElementById("integralLower").value);
        const lowerBound = lower;
        const upper = Number(document.getElementById("integralUpper").value);
        let option = document.querySelector('input[name="calculationType"]:checked').value;
        let total = 0;
        const step = 0.01;

        // getting base variable
        variables = extractVariables(equation);

        // parsing equation
        parser.evaluate(`f(${variables[0]}) = ${equation}`);

        // calculating integral
        if(option == "left"){
            while (lower < upper){
                total = total + (parser.evaluate(`f(${lower})`));
                lower = lower + step;
            }
        }else if (option == "right"){
            while (lower < upper){
                total = total + (parser.evaluate(`f(${(lower + step)})`));
                lower = lower + step;
            }
        }else{
            while(lower < upper){
                total = total + ((parser.evaluate(`f(${lower})`) + parser.evaluate(`f(${(lower + step)})`)) / 2);
                lower = lower + step;
            }
        }
        total *= step;
        document.getElementById("integralRes").value = total;

        // plotting graph for calculated area
        const inputComp = math.compile(equation);

        const xValues = math.range(lowerBound, upper + 0.01, 0.01).toArray();
        const yValues = xValues.map(function(x) {
            return inputComp.evaluate({x: x});
        });

        const trace = {
            x: xValues,
            y: yValues,
            mode: 'lines+text',
            fill: 'tonexty', // hatching calculated area
            type: 'scatter'
        }

        const layout = {
            autosize: false,
            width: 500,
            height: 500,
            title: 'Calculated Area as Integral Result',
            annotations: [ // adding label to graph
                {
                    x: (upper + lowerBound) / 2,
                    y: 10,
                    allign: "center",
                    text: `${total.toFixed(2)}`,
                    font: {size: 18},
                    showarrow: false
                }
            ]
        }

        const data = [trace];
        Plotly.newPlot('plotlyIntegral', data, layout);
    }catch(err){
        alert(err);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// evaluate equations to extract variables. fourth tab calculation
let valueMap = new Map(); // this will keep values of variables in case re-evaluate some other equations
function evaluateSolve(divID, divID2){
    try {
        let equation = document.getElementById("solutionExp").value.toString();
        let variables = [];

        // getting variables
        variables = extractVariables(equation);

        // if no variable, there is error, and do not show hidden part
        if(variables.length == 0){
            setVisibility(divID, divID2);
            throw new Error ("Could not find any variables! Try again.");
        } 

        // here adding variables to html dynamically
        let newStuff = document.getElementById(divID);
        newStuff.innerHTML = "";

        for(let i = 0; i < variables.length; i++){
            if (valueMap.get(variables[i])){
                newStuff.innerHTML += `<br><label style="color: #06477c;"> ${variables[i]} = </label>
                <input type="text" placeholder="Value of ${variables[i]}" id="${variables[i]}" size="10" class="input_text" value="${valueMap.get(variables[i])}">`
            }else{
                newStuff.innerHTML += `<br><label style="color: #06477c;"> ${variables[i]} = </label>
                <input type="text" placeholder="Value of ${variables[i]}" id="${variables[i]}" size="10" class="input_text">`
            }
        }
    }catch(err){
        alert(err);
    }
}

// solving equations by given values
function solveCalc(){
    try {
        let equation = document.getElementById("solutionExp").value.toString();
        let variableValues = [];
        let values;
        let variableNames = [];
        let names;

        // getting variables
        variableNames = extractVariables(equation);
        names = variableNames.join();
        
        // gettin values for variables
        for(let i = 0; i < variableNames.length; i++){
            variableValues.push(document.getElementById(`${variableNames[i]}`).value.toString());
            if (!valueMap.get(variableNames[i])){
                valueMap.set(variableNames[i], variableValues[i]);
            }
        }
        values = variableValues.join();

        parser.evaluate(`f(${names}) = ${equation}`);
        let result = parser.evaluate(`f(${values})`);

        document.getElementById("solutionRes").value = result;
    }catch(err){
        alert(err);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// simplify equations. fifth tab calculation
function simplifyCalc(){
    try{
        let equation = document.getElementById("simplifyExp").value.toString();
        let result = math.simplify(equation);

        document.getElementById("simplifyRes").value = result;
    }catch(err){
        alert(err);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// evaluate derivative expression to find base
function evaluateDeriv(divID, divID2){
    try{
        let equation = document.getElementById("derivativeExp").value.toString();
        let variables = [];

        // getting base variable
        variables = extractVariables(equation);
        
        // if no variable, there is error, and do not show hidden part
        if(variables.length != 1){
            setVisibility(divID, divID2);
            throw new Error ("Only one variable is needed to calculate derivative! Try again.");
        }

        // here adding variables to html dynamically
        let newStuff = document.getElementById(divID);
        newStuff.innerHTML = "";

        for(let i = 0; i < variables.length; i++){
            newStuff.innerHTML = `<label style="color: #06477c;"><b>Derivative based on "${variables[i]}"</b></label><br></br>`;
        }
    }catch(err){
        alert(err)
    }
}

// calculating derivative of equations. sixth tab calculation
function derivativeCalc(){
    try{
        let equation = document.getElementById("derivativeExp").value.toString();
        let variables = [];

        // getting base variable
        variables = extractVariables(equation);

        let result = math.derivative(math.parse(equation), math.parse(variables[0]));
        document.getElementById("derivativeRes").value = result;
    }catch(err){
        alert(err)
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// unit conversion. seventh tab calculation
function unitCalc(){
    try{
        let exp = document.getElementById("unitExp").value.toString();
        document.getElementById("unitRes").value = parser.evaluate(exp);
    }catch(err){
        alert(err)
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// conditional expression solution. eigth tab calculation
function conditionalCalc(){
    try{
        let exp = document.getElementById("conditionalExp").value.toString();
        document.getElementById("conditionalRes").value = parser.evaluate(exp);
    }catch(err){
        alert(err)
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// generic copy result function
function copyResult(result){
    try {
        const res = document.getElementById(result);
        res.select();
        document.execCommand("copy");
        alert("Result has been copied on clipboard");
    }catch(err){
        alert(err);
    }
}

//---------------------------------------------------------------------------------------------------------------------------------------
// some repeatative code blocks
function extractVariables(equation){
    let variables = [];
    for(let i = 0; i < equation.length; i++){
        if((equation.charAt(i)).match(/[a-zA-Z]+/g) && !variables.includes(equation.charAt(i))){
            variables.push(equation.charAt(i))
        }
    }
    return variables;
}

function setVisibility(divID, divID2){
    let el1 = document.getElementById(divID);
    el1.classList.remove("active");
    el1.classList.remove("hidden");
    el1.classList.add("problem");
    let el2 = document.getElementById(divID2);
    el2.classList.remove("active");
    el2.classList.remove("hidden");
    el2.classList.add("problem");
}
