/*
    Sequance between modules as below:
        1. app.js (creating GUI window)
        2. index.html (Designing and structure of GUI window elements)
        3. master.css (Detailed visual design of GUI elements)
        4. index.js (JS code for index.html file. receiving, processing and sending data)
*/
// importing modules
const electron = require("electron");
const path = require("path");
const url = require("url");

// initializing electron app and BrowserWindow
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

// global window variable. it will be used inside different functions
let win;

// creating window and setting it up
function createWindow() {
    // window itself
    win = new BrowserWindow({
        width: 850,
        height: 475,
        webPreferences: { nodeIntegration: true },
        backgroundColor: '#eeeeee'
    });

    win.setMenuBarVisibility(false);

    // setting html file in which there will be window content
    win.loadURL(url.format({
        pathname: path.join(__dirname, 'src/index.html'),
        protocol: 'file',
        slashes: true
    }));

    win.on('closed', () => {
        win = null;
    });
};

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if(process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    if(win === null) {
        createWindow();
    }
});
